//
//  DetailViewController.m
//  exactAssignment
//
//  Created by AhmedAbdulmuneemHussein Mutahar on 12/25/16.
//  Copyright © 2016 Ahmed Mutahar. All rights reserved.
//

#import "DetailViewController.h"
#import "Contact.h"
#import "ContactsDataController.h"

@interface DetailViewController ()<UITableViewDelegate, UITableViewDataSource>
{
    
    NSMutableArray *values;
}

@end

@implementation DetailViewController

#pragma mark - Managing the detail item



-(void)setContact:(Contact *)contact {
    if (_contact != contact) {
        _contact = contact;
    }
    if (contact) {
        NSLog(@"%@", contact.fullName);
    }
//    values = [self.dataController getContactsValueAsArray:self.contact withKeys:keys];
//    NSLog(@"%lu", [values count]);
//    NSLog(@"%lu", [keys count]);
}

//- (void)configureView {
    // Update the user interface for the detail item.
//    if (self.detailItem) {
//        self.detailDescriptionLabel.text = [self.detailItem description];
//    }
//}

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    [self configureView];
    self.dataController = [[ContactsDataController alloc]init];
    self.keys = [[NSMutableArray alloc]init];
    values = [[NSMutableArray alloc]init];
//    NSLog(@"view did load %@", self.contact.fullName);
//    values = [self.dataController getContactsValueAsArray:self.contact withKeys:self.keys];
    [self getContactsValueAsArray];
//    NSLog(@"values: %lu", [values count]);
//    NSLog(@"keys: %lu", [self.keys count]);
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 240;
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"detailsCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    NSString *keylabel = [self.keys objectAtIndex:indexPath.row];
    NSString *valueLabel = [values objectAtIndex:indexPath.row];
    cell.textLabel.text = keylabel;
    cell.detailTextLabel.text = valueLabel;
    return cell;
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
//    NSLog(@"this is numner of seciotns");
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    NSLog(@"this is number of rows %lu", [self.keys count]);
    return [values count];
//    return 0;
}

-(void)getContactsValueAsArray {

    if (self.contact.firstName) {
        [values addObject:self.contact.firstName];
        [self.keys addObject: @"First Name"];
    }
    if (self.contact.middleName) {
        [values addObject:self.contact.middleName];
        [self.keys addObject: @"Middle Name"];
    }
    if (self.contact.lastName) {
        [values addObject:self.contact.lastName];
        [self.keys addObject: @"Last Name"];
    }
    if (self.contact.fullName) {
        [values addObject:self.contact.fullName];
        [self.keys addObject: @"Full Name"];
    }
    if (self.contact.gender) {
        [values addObject:self.contact.gender];
        [self.keys addObject: @"Gender"];
    }
    if (self.contact.mobile) {
        [values addObject:self.contact.mobile];
        [self.keys addObject: @"Mobile"];
    }
    if (self.contact.phone) {
        [values addObject:self.contact.phone];
        [self.keys addObject: @"Phone"];
    }
    if (self.contact.businessMobile) {
        [values addObject:self.contact.businessMobile];
        [self.keys addObject: @"Business Mobile"];
    }
    if (self.contact.businessPhone) {
        [values addObject:self.contact.businessPhone];
        [self.keys addObject: @"Business Phone"];
    }
    if (self.contact.email) {
        [values addObject:self.contact.email];
        [self.keys addObject: @"Email"];
    }
    if (self.contact.businessEmail) {
        [values addObject:self.contact.businessEmail];
        [self.keys addObject: @"Business Email"];
    }
    if (self.contact.JobTitleDescription) {
        [values addObject:self.contact.JobTitleDescription];
        [self.keys addObject: @"Job Title"];
    }
    if (self.contact.notes) {
        [values addObject:self.contact.notes];
        [self.keys addObject: @"Notes"];
    }

}

@end
