//
//  MasterViewController.h
//  exactAssignment
//
//  Created by AhmedAbdulmuneemHussein Mutahar on 12/25/16.
//  Copyright © 2016 Ahmed Mutahar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ContactsApi.h"
#import "CustomIOSAlertView.h"

@class DetailViewController;
@class ContactsDataController;

@interface MasterViewController : UITableViewController <ContactsApi, UITableViewDelegate,UITableViewDataSource, CustomIOSAlertViewDelegate>

@property (strong, nonatomic) ContactsDataController *dataController;

@property (strong, nonatomic) DetailViewController *detailViewController;

@property (strong, nonatomic) CustomIOSAlertView *alertView;


@end

