//
//  AppDelegate.h
//  exactAssignment
//
//  Created by AhmedAbdulmuneemHussein Mutahar on 12/25/16.
//  Copyright © 2016 Ahmed Mutahar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

