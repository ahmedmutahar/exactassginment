//
//  ContactsApi.h
//  exactAssignment
//
//  Created by AhmedAbdulmuneemHussein Mutahar on 25/12/2016.
//  Copyright © 2016 Ahmed Mutahar. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ContactsApi <NSObject>

@required
-(void)processCompleted;

@end
