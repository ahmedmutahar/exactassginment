//
//  DetailViewController.h
//  exactAssignment
//
//  Created by AhmedAbdulmuneemHussein Mutahar on 12/25/16.
//  Copyright © 2016 Ahmed Mutahar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ContactsDataController.h"
@class Contact;

@interface DetailViewController : UITableViewController 

@property (strong, nonatomic) IBOutlet UITableView *table;

@property (strong, nonatomic) id detailItem;
@property (weak, nonatomic) IBOutlet UILabel *detailDescriptionLabel;
@property (strong, nonatomic) Contact *contact;
@property (nonatomic, strong) ContactsDataController *dataController;
@property (nonatomic, strong) NSMutableArray *keys;

-(void)getContactsValueAsArray;

@end

