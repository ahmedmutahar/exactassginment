//
//  Contact.m
//  exactAssignment
//
//  Created by AhmedAbdulmuneemHussein Mutahar on 12/25/16.
//  Copyright © 2016 Ahmed Mutahar. All rights reserved.
//

#import "Contact.h"

@implementation Contact


- (id)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    if (self) {
        self.firstName = [dict objectForKey:@"FirstName"];
        self.middleName = [dict objectForKey:@"MiddleName"];
        self.lastName = [dict objectForKey:@"LastName"];
        self.fullName = [dict objectForKey:@"FullName"];
        self.phone = [dict objectForKey:@"Phone"];
        self.businessEmail = [dict objectForKey:@"BusinessEmail"];
        self.businessMobile = [dict objectForKey:@"BusinessMobile"];
        self.businessPhone = [dict objectForKey:@"BusinessPhone"];
        self.gender = [dict objectForKey:@"Gender"];
        self.ID = [dict objectForKey:@"ID"];
        self.accountID = [dict objectForKey:@"Account"];
        self.mobile = [dict objectForKey:@"Mobile"];
        self.email = [dict objectForKey:@"Email"];
        self.imageUrl = [dict objectForKey:@"PictureThumbnailUrl"];
        self.JobTitleDescription = [dict objectForKey:@"JobTitleDescription"];
        self.notes = [dict objectForKey:@"Notes"];
        self.merge = NO;
        return self;
    }
    return nil;
}


@end
