//
//  ContactsDataController.m
//  exactAssignment
//
//  Created by AhmedAbdulmuneemHussein Mutahar on 12/25/16.
//  Copyright © 2016 Ahmed Mutahar. All rights reserved.
//

#import "ContactsDataController.h"
#import "Contact.h"

@interface ContactsDataController ()
    -(void)initializeDataList;
@end

@implementation ContactsDataController

-(void)initializeDataList {
    
//    NSMutableArray *list = [[NSMutableArray alloc] init];
    
    
//    Contact *contact;
    
    NSURL *url = [NSURL URLWithString:@"https://contacts-8d05b.firebaseio.com/.json"];
    
    
    
    [[[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        // get the data into dictionary
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
        
        NSDictionary *d = [dict objectForKey:@"d"];
        // get the results array
        NSArray *array = [d objectForKey:@"results"];
        
        // add every array to the mutable array.
        for (NSDictionary *temp in array) {
            Contact *contact = [[Contact alloc]initWithDictionary:temp];
            [self addContactListObject:contact];
            if (contact.fullName == nil) {
//                NSLog(@"this contact is nil");
            }
            NSLog(@"%@", [contact fullName]);
        }
        
        [self.delegate processCompleted];
        
    }]resume];
    
}

-(id)init {
    if (self = [super init]) {
        self.ContactList = [[NSMutableArray alloc] init];
        // just dummy data
//        [self initializeDataList];
//        NSMutableArray *list = [[NSMutableArray alloc] init];
//        self.ContactList = list;
//        Contact *contact = [[Contact alloc]init];
//        contact.fullName = @"Me";
//        contact.mobile = @"01123456789";
//        [self addContactListObject:contact];
        return self;
    }
    return nil;
}

-(void)setDelegate:(id)delegate {
    _delegate = delegate;
}

-(void)setContactList:(NSMutableArray *)newList
{
    if(_ContactList !=newList){
        _ContactList =[newList mutableCopy];
    }
}

-(NSUInteger) countOfContactList {
    return [self.ContactList count];
}

-(Contact *)objectInContactListAtIndex:(NSUInteger)index
{
    return [self.ContactList objectAtIndex:index];
}

-(void)addContactListObject:(Contact *)contact
{
    return [self.ContactList addObject:contact];
}


-(void)loadContact {
    [self initializeDataList];
}

// just in case need to use this function instead.
//convert the object property and values to array to display them in detail view controller
-(NSMutableArray *)getContactsValueAsArray:(Contact *)contact withKeys:(NSMutableArray *)keys {
    NSMutableArray *values = [[NSMutableArray alloc]init];

    if (contact.firstName) {
        [values addObject:contact.firstName];
        [keys addObject: @"First Name"];
    }
    if (contact.middleName) {
        [values addObject:contact.middleName];
        [keys addObject: @"Middle Name"];
    }
    if (contact.lastName) {
        [values addObject:contact.lastName];
        [keys addObject: @"Last Name"];
    }
    if (contact.fullName) {
        [values addObject:contact.fullName];
        [keys addObject: @"Full Name"];
    }
    if (contact.gender) {
        [values addObject:contact.gender];
        [keys addObject: @"Gender"];
    }
    if (contact.mobile) {
        [values addObject:contact.mobile];
        [keys addObject: @"Mobile"];
    }
    if (contact.phone) {
        [values addObject:contact.phone];
        [keys addObject: @"Phone"];
    }
    if (contact.businessMobile) {
        [values addObject:contact.businessMobile];
        [keys addObject: @"Business Mobile"];
    }
    if (contact.businessPhone) {
        [values addObject:contact.businessPhone];
        [keys addObject: @"Business Phone"];
    }
    if (contact.email) {
        [values addObject:contact.email];
        [keys addObject: @"Email"];
    }
    if (contact.businessEmail) {
        [values addObject:contact.businessEmail];
        [keys addObject: @"Business Email"];
    }
    if (contact.JobTitleDescription) {
        [values addObject:contact.JobTitleDescription];
        [keys addObject: @"Job Title"];
    }
    if (contact.notes) {
        [values addObject:contact.notes];
        [keys addObject: @"Notes"];
    }


    return values;
}


@end
