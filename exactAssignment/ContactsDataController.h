//
//  ContactsDataController.h
//  exactAssignment
//
//  Created by AhmedAbdulmuneemHussein Mutahar on 12/25/16.
//  Copyright © 2016 Ahmed Mutahar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ContactsApi.h"
@class Contact;
@interface ContactsDataController : NSObject

//{
//    id<ContactsApi> _delegate;
//}

@property (nonatomic, copy) NSMutableArray *ContactList;

@property (nonatomic, strong) id delegate;

-(NSUInteger)countOfContactList;
-(Contact *)objectInContactListAtIndex:(NSUInteger)index;
-(void) addContactListObject:(Contact *) contact;
-(void) loadContact;
-(NSMutableArray *)getContactsValueAsArray:(Contact *)contact withKeys:(NSMutableArray *)keys;

@end
