//
//  main.m
//  exactAssignment
//
//  Created by AhmedAbdulmuneemHussein Mutahar on 12/25/16.
//  Copyright © 2016 Ahmed Mutahar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
