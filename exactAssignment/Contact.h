//
//  Contact.h
//  exactAssignment
//
//  Created by AhmedAbdulmuneemHussein Mutahar on 12/25/16.
//  Copyright © 2016 Ahmed Mutahar. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Contact : NSObject

@property (nonatomic, copy) NSString *firstName;
@property (nonatomic, copy) NSString *middleName;
@property (nonatomic, copy) NSString *lastName;
@property (nonatomic, copy) NSString *fullName;
@property (nonatomic, copy) NSString *email;
@property (nonatomic, copy) NSString *imageUrl;
@property (nonatomic, copy) NSString *businessPhone;
@property (nonatomic, copy) NSString *businessEmail;
@property (nonatomic, copy) NSString *gender;
@property (nonatomic, copy) NSString *accountID;
@property (nonatomic, copy) NSString *ID;
@property (nonatomic, copy) NSString *JobTitleDescription;
@property (nonatomic, copy) NSString *mobile;
@property (nonatomic, copy) NSString *phone;
@property (nonatomic, copy) NSString *notes;
@property (nonatomic, copy) NSString *businessMobile;
@property (nonatomic) BOOL merge;

-(id)initWithDictionary:(NSDictionary *)dict;


@end
