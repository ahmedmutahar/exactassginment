//
//  customContactCell.h
//  exactAssignment
//
//  Created by AhmedAbdulmuneemHussein Mutahar on 27/12/2016.
//  Copyright © 2016 Ahmed Mutahar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface customContactCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imageViewContact;
@property (weak, nonatomic) IBOutlet UILabel *textLabelContact;
@property (weak, nonatomic) IBOutlet UILabel *detailedTextLabel;
@property (weak, nonatomic) IBOutlet UIButton *mergeMe;


@end
