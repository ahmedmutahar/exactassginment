//
//  MasterViewController.m
//  exactAssignment
//
//  Created by AhmedAbdulmuneemHussein Mutahar on 12/25/16.
//  Copyright © 2016 Ahmed Mutahar. All rights reserved.
//

#import "MasterViewController.h"
#import "DetailViewController.h"
#import "ContactsDataController.h"
#import "Contact.h"
#import "UIImageView+AFNetworking.h"
#import "customContactCell.h"

@interface MasterViewController ()

@property NSMutableArray *objects;
-(void) mergeObjectAtIndex:(NSUInteger)index with:(Contact *)contact;
-(void) launchDialogForContact:(Contact *)mainContact and:(Contact *)contact;
- (UIView *)createDialogViewWithContact:(Contact *)mainContact and:(Contact *)contact;
-(void)showMergeButton;
-(void)doneToast;
@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong) UIImageView *imageView2;
@property (nonatomic) Contact *currentContact;



@end

@implementation MasterViewController


-(void)awakeFromNib {
    
    self.dataController = [[ContactsDataController alloc] init];
    [self.dataController setDelegate:self];
    [self.dataController loadContact];
    [super awakeFromNib];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.objects =  self.dataController.ContactList;


    UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithTitle:@"merge" style:UIBarButtonItemStylePlain target:self action:@selector(mergeContacts:)];
    self.navigationItem.rightBarButtonItem = addButton;
    self.detailViewController = (DetailViewController *)[[self.splitViewController.viewControllers lastObject] topViewController];

    
}

- (void)viewWillAppear:(BOOL)animated {
    self.clearsSelectionOnViewWillAppear = self.splitViewController.isCollapsed;
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//- (void)insertNewObject:(id)sender {
//    if (!self.objects) {
//        self.objects = [[NSMutableArray alloc] init];
//    }
//    [self.objects insertObject:[NSDate date] atIndex:0];
//    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
//    [self.tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
//}

#pragma mark - merge
- (void) mergeContacts:(id)sender {
    NSLog(@"merging contacts");
    
    [self showMergeButton];
    
    NSLog(@"merging is done");
//    NSLog(@"%@", self.dataController.ContactList);
    [self.tableView reloadData];
    
}

-(void)showMergeButton {
    for (int i = 0; i <[self.dataController countOfContactList]; i++) {
        Contact *contact = [self.dataController objectInContactListAtIndex:i];
        for (int j = 0; j < [self.dataController countOfContactList]; j++) {
            Contact *secondContact = [self.dataController objectInContactListAtIndex:j];
            if (contact.ID && ![contact.ID isEqualToString:secondContact.ID]) {
                
                if (!contact.merge) {
                    if ([contact.fullName isEqualToString:secondContact.fullName]) {
                        // enable the merge button
                        NSLog(@"%@, %@", contact.ID, contact.fullName);
                        NSLog(@"%@, %@", secondContact.ID, secondContact.fullName);
                        contact.merge = YES;
                    } else if ([contact.businessEmail isEqualToString:secondContact.businessEmail] || [contact.email isEqualToString:secondContact.email]) {
                        // enable the merge button
                        NSLog(@"%@, %@", contact.ID, contact.email?contact.email:contact.businessEmail);
                        NSLog(@"%@, %@", secondContact.ID, secondContact.email?secondContact.email:secondContact.businessEmail);
                        contact.merge = YES;
                    }
                }
            }
        }
    }
}

-(void)mergeObjectAtIndex:(NSUInteger)index with:(Contact *)contact {
    self.currentContact = [self.dataController objectInContactListAtIndex:index];
    NSLog(@"full name is %@", self.currentContact.fullName);
    if (!self.currentContact.firstName) {
        self.currentContact.firstName = contact.firstName?[contact.firstName copy]:@"";
    }
    if (!self.currentContact.middleName) {
        self.currentContact.middleName = contact.middleName?[contact.middleName copy]:@"";
    }
    if (!self.currentContact.lastName) {
        self.currentContact.lastName = contact.lastName?[contact.lastName copy]:@"";
    }
    if (!self.currentContact.fullName) {
        self.currentContact.fullName = contact.fullName?[contact.fullName copy]:@"";
    }
    if (!self.currentContact.email) {
        self.currentContact.email = contact.email?[contact.email copy ]:@"";
    }
    if(!self.currentContact.businessEmail){
        self.currentContact.businessEmail = contact.businessEmail?[contact.businessEmail copy ]:@"";
    }
    if (!self.currentContact.businessMobile) {
        self.currentContact.businessMobile = contact.businessMobile?[contact.businessMobile copy ]:@"";
    }
    if (!self.currentContact.businessPhone) {
        self.currentContact.businessPhone = contact.businessPhone?[contact.businessPhone copy ]:@"";
    }
    if (!self.currentContact.phone) {
        self.currentContact.phone = contact.phone?[contact.phone copy ]:@"";
    }
    // Gender is known M or F
    if (!self.currentContact.gender) {
        self.currentContact.gender = contact.gender?[contact.gender copy ]:@"";
    } else if ([contact.gender isEqualToString:@"M"] || [contact.gender isEqualToString:@"F"]) {
        self.currentContact.gender = [contact.gender copy];
    }
    if (!self.currentContact.JobTitleDescription) {
        self.currentContact.JobTitleDescription = contact.JobTitleDescription?[contact.JobTitleDescription copy ]:@"";
    } else if (contact.JobTitleDescription){
        [self.currentContact.JobTitleDescription stringByAppendingString:@"\r"];
        [self.currentContact.JobTitleDescription stringByAppendingString:contact.JobTitleDescription];
    }
    if (!self.currentContact.mobile) {
        self.currentContact.mobile = contact.mobile?[contact.mobile copy ]:@"";
    }
    if (self.currentContact.notes) {
        self.currentContact.notes = contact.notes?[contact.notes copy ]:@"";
    } else if(contact.notes){
        [self.currentContact.notes stringByAppendingString:@"\r"];
        [self.currentContact.notes stringByAppendingString:contact.notes];
    }
    
    // in case accountIDs are the same then just let user choose which image if both contacts have image
    if (!self.currentContact.imageUrl || [self.currentContact.imageUrl isEqualToString:@""]) {
        self.currentContact.imageUrl = contact.imageUrl?[contact.imageUrl copy ]:@"";
        NSLog(@"current contact :( %@", self.currentContact.fullName);
        NSLog(@"the deleted contact :( is %@", contact.fullName);
        self.currentContact.merge = NO;
        [self.dataController.ContactList removeObject:contact];
        [self showMergeButton];
        [self.tableView reloadData];
        [self doneToast];
    } else if (contact.imageUrl){
        [self launchDialogForContact:self.currentContact and:contact];
    } else {
        NSLog(@"current contact %@", self.currentContact.fullName);
        NSLog(@"the deleted contact is %@", contact.fullName);
        self.currentContact.merge = NO;
        [self.dataController.ContactList removeObject:contact];
        [self showMergeButton];
        [self.tableView reloadData];
        [self doneToast];
    }
    
}

-(void)launchDialogForContact:(Contact *)mainContact and:(Contact *)contact
{
    self.alertView = [[CustomIOSAlertView alloc]init];
    //create container view
    
    [self.alertView setContainerView: [self createDialogViewWithContact:mainContact and:contact]];
    
    [self.alertView setButtonTitles:[NSMutableArray arrayWithObjects:@"Close", nil]];
    [self.alertView setDelegate:self];
    
//    MasterViewController * __weak weakSelf = self; // no need for that for now
    
    // use a Block, rather than a delegate.
    [self.alertView setOnButtonTouchUpInside:^(CustomIOSAlertView *alertView, int buttonIndex) {

        [alertView close];
    }];
    
    [self.alertView setUseMotionEffects:true];
    
    // And launch the dialog
    [self.alertView show];
    
}

#pragma mark - Segues

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"showDetail"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];

        Contact *contact = self.dataController.ContactList[indexPath.row];
        DetailViewController *controller = (DetailViewController *)[[segue destinationViewController] topViewController];

        [controller setContact:contact];
        controller.navigationItem.leftBarButtonItem = self.splitViewController.displayModeButtonItem;
        controller.navigationItem.leftItemsSupplementBackButton = YES;
    }
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    NSLog(@"number of rows %lu", [self.dataController countOfContactList]);
//    return [self.dataController countOfContactList];
    return self.objects.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    
    static NSString *CellIdentifier = @"ContactCell";
    
    
//    customContactCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
//    customContactCell *cell = (customContactCell *)[tableView cellForRowAtIndexPath:indexPath];
    customContactCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    if (cell == nil) {
        cell = [[customContactCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.imageViewContact.image = [UIImage imageNamed:@"noImage"];

    Contact *contact = [self.dataController.ContactList objectAtIndex:indexPath.row];


    // set Merge buttons
    if (contact.merge == YES) {
        cell.mergeMe.tag = indexPath.row;
        cell.mergeMe.hidden = NO;
        [cell.mergeMe addTarget:self action:@selector(mergeMeAction:) forControlEvents:UIControlEventTouchUpInside];
        
    } else {
        cell.mergeMe.hidden = YES;
    }
    
    
    
    //display the title with full name OR first name + last name OR first name + middle Name + last name
    if (contact.fullName == nil || [contact.fullName isEqualToString:@""]) {
        NSString *name = @"";
        if (contact.firstName !=nil) {
            name = [name stringByAppendingString:contact.firstName];
        }
        if (contact.middleName !=nil) {
            name = [name stringByAppendingString:contact.middleName];
        }
        if (contact.lastName != nil) {
            name = [name stringByAppendingString:contact.lastName];
        }

        cell.textLabelContact.text = name;
        
    } else {
        cell.textLabelContact.text = contact.fullName;
    }
    
    //display the subtitle with mobile number or email
    if (contact.businessMobile == nil) {
        if (contact.businessPhone == nil) {
            if (contact.mobile == nil) {
                if (contact.phone == nil) {
                    cell.detailedTextLabel.text = contact.email?contact.email:contact.businessEmail?contact.businessEmail:@"";
//                    NSLog(@"No phone Number");
                }else {
                    cell.detailedTextLabel.text = contact.phone;
                }
                
            } else {
                cell.detailedTextLabel.text = contact.mobile;
            }
        } else {
            cell.detailedTextLabel.text = contact.businessPhone;
        }
    } else {
        cell.detailedTextLabel.text = contact.businessMobile;
    }
    

    
    if (contact.imageUrl !=nil) {
        [cell.imageViewContact setClipsToBounds:YES];
        [cell.imageViewContact setAutoresizingMask:UIViewAutoresizingNone];
        [cell.imageViewContact setImageWithURL:[NSURL URLWithString:contact.imageUrl]];
    }
    
    
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return NO;
}

-(void)processCompleted {
    
    NSLog(@"Master process completed!");
    
    
    [self.tableView reloadData];
    
    // this could help solve the problem of not showing the table at the initial stage.
//    [[self tableView] beginUpdates];
//    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[self.dataController countOfContactList] inSection:0];
//    [self.tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    
}

-(UIView *)createDialogViewWithContact:(Contact *)mainContact and:(Contact *)contact {
    CGFloat image1x = 20;
    CGFloat image1y = 50;
    CGFloat imagew = 120;
    CGFloat imageh = 120;
    CGFloat image2x = image1x + 10 + imagew;
    CGFloat image2y = image1y;
    CGFloat label1x = image1x;
    CGFloat label1y = imageh+ image1y;
    CGFloat labelw = imagew;
    CGFloat labelh = 40;
    CGFloat label2x = image2x;
    CGFloat label2y = imageh+image1y;
    
    
    UIView *demoView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 290, 220)];
    
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 10, 290, 40)];
    if (mainContact.fullName) {
        [titleLabel setText:[NSString stringWithFormat:@"Choose Image for %@", mainContact.fullName]];
    } else {
        [titleLabel setText:[NSString stringWithFormat:@"Choose Image for %@ %@", mainContact.firstName, mainContact.lastName]];
    }
//    [titleLabel setText:@"Choose Image for Full Name"];
    [titleLabel setTextAlignment:NSTextAlignmentCenter];
    
    self.imageView = [[UIImageView alloc] initWithFrame:CGRectMake(image1x, image1y, imagew, imageh)];
    [self.imageView setImageWithURL:[NSURL URLWithString:mainContact.imageUrl] placeholderImage:[UIImage imageNamed:@"noImage"]];
//    [self.imageView setImage:[UIImage imageNamed:@"demo"]];
    self.imageView2 = [[UIImageView alloc]initWithFrame:CGRectMake(image2x, image2y, imagew, imageh)];
    [self.imageView2 setImageWithURL:[NSURL URLWithString:contact.imageUrl] placeholderImage:[UIImage imageNamed:@"noImage"]];
//    [self.imageView2 setImage:[UIImage imageNamed:@"demo"]];
    
    UILabel *firstLabel = [[UILabel alloc]initWithFrame:CGRectMake(label1x, label1y, labelw, labelh)];
    [firstLabel setText:@"image 1"];
    [firstLabel setTextAlignment:NSTextAlignmentCenter];
    
    UILabel *secondLabel = [[UILabel alloc]initWithFrame:CGRectMake(label2x, label2y, labelw, labelh)];
    [secondLabel setText:@"image 2"];
    [secondLabel setTextAlignment:NSTextAlignmentCenter];
    
    UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(imageTapped:)];
    tap1.numberOfTapsRequired = 1;
    tap1.numberOfTouchesRequired = 1;
    UITapGestureRecognizer *tap2 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(imageTapped:)];
    tap2.numberOfTapsRequired = 1;
    tap2.numberOfTouchesRequired = 1;
    [self.imageView addGestureRecognizer:tap1];
    [self.imageView setUserInteractionEnabled:YES];
    // set the tag to other contact
    self.imageView.tag = [self.dataController.ContactList indexOfObject:contact];
    [self.imageView2 addGestureRecognizer:tap2];
    [self.imageView2 setUserInteractionEnabled:YES];
    self.imageView2.tag = [self.dataController.ContactList indexOfObject:contact];
    
    [demoView addSubview:titleLabel];
    [demoView addSubview:self.imageView];
    [demoView addSubview:self.imageView2];
    [demoView addSubview:firstLabel];
    [demoView addSubview:secondLabel];
    
    return demoView;
}

-(void)imageTapped:(UITapGestureRecognizer *)gestureRecognizer {
    
    //    NSLog(@"%@", [gestureRecognizer view]);
    int tag = [[gestureRecognizer view]tag];
    Contact *contact = [self.dataController.ContactList objectAtIndex:tag];
    if ([gestureRecognizer view] == self.imageView) {
        NSLog(@"image 1 clicked");
        NSLog(@"current contact %@", self.currentContact.fullName);
        NSLog(@"to be deleted %@", contact.fullName);
        self.currentContact.merge = NO;
        [self.dataController.ContactList removeObjectAtIndex:tag];
        [self showMergeButton];     // recalculate if the contact has another merge options
        [self.tableView reloadData];
//        NSLog(@"%lu", [[gestureRecognizer view]tag]);
        [self.alertView close];
        [self doneToast];
    } else if ([gestureRecognizer view] == self.imageView2){
        NSLog(@"image 2 clicked");
        NSLog(@"current contact %@", self.currentContact.fullName);
        NSLog(@"to be deleted %@", contact.fullName);
        self.currentContact.merge = NO;
        self.currentContact.imageUrl = [contact.imageUrl copy];
        [self.dataController.ContactList removeObjectAtIndex:tag];
        [self showMergeButton];
        
        [self.tableView reloadData];
//        NSLog(@"%lu", [[gestureRecognizer view]tag]);
        
        [self.alertView close];
        [self doneToast];
    }
}


// interface for close button.. won't be used
- (void)customIOS7dialogButtonTouchUpInside: (CustomIOSAlertView *)alertView clickedButtonAtIndex: (NSInteger)buttonIndex
{
    // to be implemented if needed!
}

- (void)mergeMeAction:(id)sender {
    int tag = [(UIButton *)sender tag];
    NSLog(@"%d", tag);
    
    Contact *contact = [self.dataController.ContactList objectAtIndex:tag];
    NSLog(@"%@", contact.fullName);
    for (Contact *cntct in self.dataController.ContactList) {
        if (contact.ID && ![contact.ID isEqualToString:cntct.ID]) {
            if ([contact.fullName isEqualToString:cntct.fullName]) {
                NSLog(@"found similiar full name");
                [self mergeObjectAtIndex:tag with:cntct];
                break;
            }else if ([contact.businessEmail isEqualToString:cntct.businessEmail] || [contact.email isEqualToString:cntct.email] ) {
                NSLog(@"found similar email %@", cntct.email);
                NSLog(@"full name is %@", cntct.fullName);
                // do the merging based on emails or business email
                [self mergeObjectAtIndex:tag with:cntct];
                break;
                
            }
        }
    }
}

-(void)doneToast {
//    NSString *message = @"Contacts Merged!";
//    
//    UIAlertView *toast = [[UIAlertView alloc] initWithTitle:nil
//                                                    message:message
//                                                   delegate:nil
//                                          cancelButtonTitle:nil
//                                          otherButtonTitles:nil, nil];
//    [toast show];
//    
//    int duration = 1; // duration in seconds
//    
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, duration * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
//        [toast dismissWithClickedButtonIndex:0 animated:YES];
//    });
    
    NSString *message = @"Contacts Merged!!!";
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    [self presentViewController:alert animated:YES completion:nil];
    
    int duration = 1; // duration in seconds
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, duration * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [alert dismissViewControllerAnimated:YES completion:nil];
    });
}

@end
